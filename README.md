## Hello

Get started command, just type in console :D
```cmd
npm build
```

Prefered IDE: WebStorm, VS Code, IntelliJ

## Command line used create this project:

```bash
npm init

ts-init

npm install eslint --save-dev

// create src/index.ts file 

./node_modules/.bin/eslint --init
```

eslint https://github.com/airbnb/javascript
